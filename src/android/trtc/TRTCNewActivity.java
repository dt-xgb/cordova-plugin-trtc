package hewz.plugins.trtc;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.tencent.trtc.TRTCCloudDef;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class TRTCNewActivity extends Activity {
    private final static int REQ_PERMISSION_CODE = 0x1000;
    private MediaPlayer mMediaPlayer;
    private Timer timer;
    private String requestId;
    private Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        final FakeR R = new FakeR(this);
        setContentView(R.getId("layout", "new_activity"));
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                finish();
            }
        };
        timer.schedule(timerTask, 30000);
        ring();
        checkPermission();

        i = getIntent();
        requestId = i.getStringExtra("requestId");
        if (!TextUtils.isEmpty(i.getStringExtra("userIcon"))) {
            Picasso.with(getApplicationContext())
                    .load(i.getStringExtra("userIcon"))
                    .resize(120, 120)
                    .transform(new CircleTransform())
                    .centerCrop()
                    .into((ImageView) findViewById(R.getId("id", "headicon")));
        }

        ((TextView) findViewById(R.getId("id", "caller"))).setText(i.getStringExtra("userName") + "通过电子班牌发起视频通话");

        findViewById(R.getId("id", "tv_enter")).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i.getIntExtra("roomId", 0) > 0) {
                    final Intent intent = new Intent(TRTCNewActivity.this.getContext(), TRTCMainActivity.class);
                    intent.putExtra("roomId", i.getIntExtra("roomId", 0));
                    intent.putExtra("userId", i.getStringExtra("userId"));
                    intent.putExtra("userName", i.getStringExtra("userName"));
                    intent.putExtra("userIcon", i.getStringExtra("userIcon"));
                    intent.putExtra("AppScene", TRTCCloudDef.TRTC_APP_SCENE_VIDEOCALL);
                    intent.putExtra("customVideoCapture", false);
                    intent.putExtra("sdkAppId", Integer.parseInt(getString(R.getId("string", "trtc_app_id"))));
                    intent.putExtra("userSig", i.getStringExtra("userSig"));
                    intent.putExtra("requestId", requestId);
                    startActivity(intent);
                    finish();
                }
            }
        });

        findViewById(R.getId("id", "tv_close")).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncHttp(null).execute("ad/trtc/reply/" + requestId + "/1");
                finish();
            }
        });
        IntentFilter filter = new IntentFilter("finishVideoCall");
        filter.addAction("VideoUpdate");
        registerReceiver(receiver, filter);
    }

    private void ring() {
        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(this, alert);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
        mMediaPlayer.setLooping(true);
        try {
            mMediaPlayer.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mMediaPlayer.start();
    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("VideoUpdate".equals(intent.getAction())) {
                i = intent;
            } else {
                Toast.makeText(getApplicationContext(), "视频通话已结束", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    };


    @Override
    protected void onDestroy() {
        if (mMediaPlayer != null && mMediaPlayer.isPlaying())
            mMediaPlayer.stop();
        timer.cancel();
        timer.purge();
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    private Context getContext() {
        return this;
    }

    private boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> permissions = new ArrayList<String>();
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)) {
                permissions.add(Manifest.permission.CAMERA);
            }
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO)) {
                permissions.add(Manifest.permission.RECORD_AUDIO);
            }
            if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (permissions.size() != 0) {
                ActivityCompat.requestPermissions(TRTCNewActivity.this,
                        (String[]) permissions.toArray(new String[0]),
                        REQ_PERMISSION_CODE);
                return false;
            }
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_PERMISSION_CODE:
                for (int ret : grantResults) {
                    if (PackageManager.PERMISSION_GRANTED != ret) {
                        Toast.makeText(getContext(), "用户没有允许需要的权限，使用可能会受到限制！", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                break;
        }
    }

    class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());
            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;
            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }
            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);
            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);
            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }
}
